name = "xnview"
version = "0.72.0"
authors = ["XnSoft"]
description = "XnViewMP is the enhanced version to XnView."
tools = ["xnview"]
# requires = ["python"]
# help = "file://{root}/help.html"
uuid = "977158e1-8b83-4a46-aa77-3e3a3e924316"
def commands():
    env.PATH.append("$BD_SOFTS/xnview/{version}")
